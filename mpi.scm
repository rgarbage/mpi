(define-module (mpi)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages pkg-config))

(define (make-package n)
  (package
    (name (string-append "mpi-" n))
    (version "0.1")
    (source
     (local-file "./src" (string-append name "-" version "source") #:recursive? #t))
    (description "")
    (synopsis "")
    (license license:gpl3+)
    (home-page "")
    (build-system gnu-build-system)
    (arguments
     (list
      #:tests? #f
      #:phases #~(modify-phases %standard-phases
                   (delete 'configure)
                   (replace 'build
                     (lambda _
                       (invoke "gcc" "-lmpi" "-o" #$n (string-append #$n ".c"))))
                   (replace 'install
                     (lambda _
                       (install-file #$n (string-append #$output "/bin"))
                       #t)))))
    (inputs (list openmpi))))

(define-public mpi-helloworld
  (make-package "helloworld"))

(define-public mpi-intercomm
  (make-package "intercomm"))

(define-public mpi-intercomm-async
  (make-package "intercomm-async"))
