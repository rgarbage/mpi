#include <mpi.h>
#include <stdio.h>
#include <unistd.h>

#define SIZE 100 /* Buffer size */

int main( int argc, char *argv[] )
{
  int rank = 0;
  int size = 0;
  char buffer[SIZE+1] = { '\0' };
  char hostname[SIZE+1] = { '\0' };
  int tag = 5;
  gethostname(&hostname, SIZE);
  MPI_Init( &argc, &argv );
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  sprintf(&buffer, "a message from %d (running on %s)", rank, &hostname);
  MPI_Send(&buffer, SIZE, MPI_CHAR, (rank+1)%size, tag, MPI_COMM_WORLD);
  printf( "Hello, world!\nI am a rank %d process running from %s and my size is %d, I sent %s, ", rank, &hostname, size, &buffer);
  MPI_Recv(&buffer, SIZE, MPI_CHAR, (rank-1)%size, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  printf("I received %s\n", buffer);
  MPI_Finalize();
  return 0;
}
