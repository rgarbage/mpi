#include <mpi.h>
#include <stdio.h>
#include <string.h>

#define SIZE 100 /* Buffer size */

int main( int argc, char *argv[] )
{
  int rank = 0;
  int size = 0;
  char buffer[SIZE+1] = { '\0' };
  int tag = 5;
  int msg_size = 10;
  int send_rank = 0;
  int recv_rank = 1;
  MPI_Init( &argc, &argv );
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  printf( "Hello, world!\nI am a rank %d process and my size is %d\n", rank, size );

  if (rank == send_rank) {
    char *msg = "a message";
    strcpy(&buffer, msg);
    MPI_Send(&buffer, msg_size, MPI_CHAR, recv_rank, tag, MPI_COMM_WORLD);
    printf("I sent %s\n", msg);
  }
  if (rank == recv_rank) {
    MPI_Recv(&buffer, msg_size, MPI_CHAR, send_rank, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    printf("I received %s\n", &buffer);
  }
  MPI_Finalize();
  return 0;
}
